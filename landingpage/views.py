from django.shortcuts import render, redirect
from . import forms, models
from django.contrib import messages

def index(request):
    if request.method == "POST":
        form = forms.EmailForm(request.POST)
        if form.is_valid(): 
            # print(form)
            form.save()
            messages.success(request, 'Profile details updated.')
            return redirect('/#')
    else :
        form = forms.EmailForm()
    em = {
        'form' : form,
    }
    return render(request, 'index.html', em)



